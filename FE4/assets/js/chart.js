var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
        datasets: [{
            label: 'Borrowed Books, Returned Books',
            data: [5, 15, 8, 6, 5, 10],
            backgroundColor: [
                'rgba(0, 0, 0 ,1)',
                'rgba(204, 204, 204 ,1)',
                'rgba(0, 0, 0 ,1)',
                'rgba(204, 204, 204 ,1)',
                'rgba(0, 0, 0 ,1)',
                'rgba(204, 204, 204,1)'
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var pie = document.getElementById('pie').getContext('2d');
var myChart = new Chart(pie, {
    type: 'pie',
    data: {
        labels: ['Novel', 'Fiction', 'Horror', 'Suspense'],
        datasets: [{
            label: '# of Votes',
            data: [10, 8, 7, 5],
            backgroundColor: [
                'rgba(102, 204, 255 ,1)',
                'rgba(140, 26, 255 ,1)',
                'rgba(77, 153, 0,1.0)',
                'rgba(255, 26, 26 ,1)'
                
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});



